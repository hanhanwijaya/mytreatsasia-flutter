import 'package:asia/data/remote/models/response/Treats_Data_Remote/free_treats_data_remote.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/banner_list_bloc.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/category_list_bloc.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/event_list_bloc.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/free_treats_list_bloc.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/merchant_list_bloc.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/promotion_nearby_list_bloc.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/super_deals_list_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:asia/data/remote/endpoints/endpoints.dart';
import 'package:asia/presentation/ui/random_team/random_team_bloc.dart';
import 'package:asia/presentation/ui/team_detail/detail_team_bloc.dart';
import 'package:asia/presentation/ui/team_list/list_team_bloc.dart';

List<Dependency> dependencies = [
  Dependency((i) => Endpoints()),
];

List<Bloc> blocs = [
  Bloc((i) => ListTeamBloc(i.get<Endpoints>())),
  Bloc((i) => DetailTeamBloc(i.get<Endpoints>())),
  Bloc((i) => RandomTeamBloc(i.get<Endpoints>())),
  Bloc((i) => BannerListBloc(i.get<Endpoints>())),
  Bloc((i) => FreeTreatsListBloc(i.get<Endpoints>())),
  Bloc((i) => PromotionNearbyListBloc(i.get<Endpoints>())),
  Bloc((i) => CategoryListBloc(i.get<Endpoints>())),
  Bloc((i) => SuperDealsListBloc(i.get<Endpoints>())),
  Bloc((i) => EventListBloc(i.get<Endpoints>())),
  Bloc((i) => MerchantListBloc(i.get<Endpoints>())),
];
