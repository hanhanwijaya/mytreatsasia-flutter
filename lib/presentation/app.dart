import 'package:flutter/material.dart';
import 'package:asia/presentation/navigation/navigation.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'navigation/navigation.dart' as router;
import 'package:shared_preferences/shared_preferences.dart';

class TeamApp extends StatelessWidget {
  @override

  Future<bool> isFirstLaunch() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstLaunch = prefs.getString('isFirstLaunch') == null
    ? true : false ;
    return isFirstLaunch;
  }


  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    return FutureBuilder(
        future: isFirstLaunch(),
        builder: (context, snapshot){
      return
        snapshot.hasData ?
        MaterialApp(
        title: 'Flutter Team App',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.deepPurple,
        ),
        initialRoute: (snapshot.data) ? routeOnBoardingScreen : routeBottomNavigationBar,
        onGenerateRoute: router.generateRoute,
      )
      : Container()
      ;
    });

  }
}
