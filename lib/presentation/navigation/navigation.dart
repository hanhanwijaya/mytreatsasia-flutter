import 'package:asia/pages/bottom_appBar.dart';
import 'package:asia/presentation/ui/onboarding_screen.dart';
import 'package:asia/presentation/ui/bottom_navigation_bar/bottom_navigation_bar.dart';
import 'package:asia/presentation/ui/treats_Page/treats_list_page.dart';
import 'package:flutter/material.dart';
import 'package:asia/presentation/ui/random_team/random_team_page.dart';
import 'package:asia/presentation/ui/team_detail/team_detail_page.dart';
import 'package:asia/presentation/ui/team_list/team_list_page.dart';

/// Navigation Class

const routeOnBoardingScreen = "/";
const routeTeamList = "/team_list";
const routeTeamDetail = "/team_detail";
const routeTeamRandom = "/team_random";

const routeBottomNavigationBar = "/bottom_navigation_bar";

const routeTreatsBasePage = "/treats_Page";


Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {

    case routeOnBoardingScreen:
      return MaterialPageRoute(builder: (context) => OnBoardingScreenPage());


    case routeTreatsBasePage:
      return MaterialPageRoute(builder: (context) => TreatsBasePage());











    case routeTeamList:
      return MaterialPageRoute(builder: (context) => TeamListPage());

    case routeBottomNavigationBar:
      return MaterialPageRoute(builder: (context) => BottomBar());

    case routeTeamDetail:
      String teamId = settings.arguments as String;
      return MaterialPageRoute(
        builder: (context) => TeamDetailPage(
          teamId: teamId,
        ),
      );

    case routeTeamRandom:
      return MaterialPageRoute(builder: (context) => RandomTeamPage());
  }
}
