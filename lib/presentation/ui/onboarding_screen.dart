import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:asia/presentation/navigation/navigation.dart' as router;
import 'package:asia/presentation/navigation/navigation.dart';

class OnBoardingScreenPage extends StatefulWidget {
  @override
  _OnBoardingScreenPageState createState() => _OnBoardingScreenPageState();
}

class _OnBoardingScreenPageState extends State<OnBoardingScreenPage> {
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  int counter = 0;


  setCon (String con) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('isFirstLaunch', con);
  }


  @override
  Widget build(BuildContext context) {



      return Scaffold(
          primary: false,
          body: Container(
            height: double.infinity,
            child: Stack(
              children: <Widget>[
                PageView(
                  controller: _pageController,
                  onPageChanged: (int page) {
                    _currentPage = page;
                    setState(() {});
                  },
                  children: <Widget>[
                    _buildPageContent(
                      image: 'images/treatspage/slide2.svg',
                      title: 'More Merchants',
                      body:
                      'A Thousand merchants has joined MyTreats as a partner!',
                    ),
                    _buildPageContent(
                      image: 'images/onboardingscreen/slide2.svg',
                      title: 'More Promos',
                      body: 'You can enjoy more than 1 Million promos everyday!',
                    ),
                    _buildPageContent(
                      image: 'images/onboardingscreen/slide2.svg',
                      title: 'More Free Vouchers',
                      body:
                      'Get free vouchers everyday as want as you need everywhere',
                    ),
                  ],
                ),
                Positioned(
                  top:  MediaQuery.of(context).size.height*0.85,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: SmoothPageIndicator(
                        controller: _pageController,
                        count: 3,
                        effect: WormEffect(),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0.0,
                  left: 0.0,
                  right: 10.0,
                  child: AppBar(

                    brightness: Brightness.light,
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    actions: <Widget>[
                      InkWell(
                          onTap: () {
                            _pageController.animateToPage(2,
                                duration: Duration(microseconds: 400),
                                curve: Curves.linear);
                          },
                          child: FlatButton(
                              splashColor: Colors.deepPurple[50],
                              child: Center(
                                  child: Text(
                                    'Skip',
                                    style: TextStyle(
                                        color: Color(0xffA0A4A8), fontSize: 16),
                                  ))))
                    ],
                  ),
                ),
              ],
            ),
          ),
          bottomSheet: _currentPage != 2
              ? Container(
            margin: EdgeInsets.symmetric(vertical: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    _pageController.animateToPage(_currentPage + 1,
                        duration: Duration(microseconds: 400),
                        curve: Curves.linear);
                  },
                  splashColor: Colors.deepPurple[50],
                  child: Text(
                    'NEXT',
                    style: TextStyle(
                        color: Color(0xFF303336),
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Icon(Icons.arrow_forward),
              ],
            ),
          )
              : Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: Container(
              height: 65,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
//                            print('cek');
                        setCon('cekCon');
                        Navigator.of(context).pushNamed(router.routeBottomNavigationBar);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight,
                              stops: [
                                0.2,
                                1,
                              ],
                              colors: [
                                Color(0xff563399),
                                Color(0xff9E85BF),
                              ]),
                          borderRadius: BorderRadius.circular(8),
//                              color: Colors.blue,
                        ),
                        width: 343,
                        height: 49,
                        alignment: Alignment.center,
                        child: Text(
                          'Launch MyTreats',
                          style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
      );
    }
  }



  Widget _buildPageContent({String image, String title, String body}) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 129.0),
              child: Center(
                child: Container(
                  width: 220,
                    height: 220,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color.fromRGBO(133, 92, 214, 0.24)
                    ),
                    child: ClipOval(
                      child: SvgPicture.asset(
                        image,
                         fit: BoxFit.cover,
                      ),
                    )),
              ),
            ),
            SizedBox(
              height: 48.0,
            ),
            Center(
              child: Text(
                title,
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff303336)),
              ),
            ),
            SizedBox(
              height: 24.0,
            ),
            Center(
              child: Container(
                width: 233.0,
                child: Text(
                  body,
                  style: TextStyle(fontSize: 16, height: 2),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }



