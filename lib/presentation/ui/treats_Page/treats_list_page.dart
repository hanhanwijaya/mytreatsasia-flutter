import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Page/category_list_page.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Page/event_list_page.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Page/free_treats_list_page.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Page/promotion_nearby_list_page.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Page/super_deals_list_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'Details_Page/List_Page/banner_list_page.dart';
import 'Details_Page/List_Page/header_list_page.dart';
import 'Details_Page/List_Page/merchant_list_page.dart';
import 'Details_Page/List_Page/promo_keeped_page.dart';

class TreatsBasePage extends StatefulWidget {
  final String lat;
  final String long;

  TreatsBasePage({
    this.lat,
    this.long,
  });

  @override
  _TreatsBasePageState createState() => _TreatsBasePageState();
}

class _TreatsBasePageState extends State<TreatsBasePage> {
//  List<String> items = ["1", "2", "3", "4", "5", "6", "7", "8"];
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length + 1).toString());
    if (mounted) setState(() {});
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        primary: false,
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 0.0,
              left: 0.0,
              right: 0.0,
              child: AppBar(
                primary: false,
                automaticallyImplyLeading: false,
                brightness: Brightness.light,
                backgroundColor: Colors.transparent,
                elevation: 0,
              ),
            ),
            SmartRefresher(
              enablePullDown: true,
              header: WaterDropHeader(),
              footer: CustomFooter(
                builder: (BuildContext context, LoadStatus mode) {
                  Widget body;
                  if (mode == LoadStatus.idle) {
                    body = Text("pull up load");
                  } else if (mode == LoadStatus.loading) {
                    body = CupertinoActivityIndicator();
                  } else if (mode == LoadStatus.failed) {
                    body = Text("Load Failed!Click retry!");
                  } else if (mode == LoadStatus.canLoading) {
                    body = Text("release to load more");
                  } else {
                    body = Text("No more Data");
                  }
                  return Container(
                    height: 100.0,
                    child: Center(child: body),
                  );
                },
              ),
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,

              child: CustomScrollView(
                physics: BouncingScrollPhysics(),
                slivers: <Widget>[
                  SliverFixedExtentList(
                    itemExtent: 30.0,
                    delegate: SliverChildListDelegate(
                      [
                        Container(
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                  SliverFixedExtentList(
                    itemExtent: 65.0,
                    delegate: SliverChildListDelegate(
                      [
                        HeaderListPage(),
                      ],
                    ),
                  ),
                  SliverPersistentHeader(
                    pinned: true,
                    delegate: _SliverAppBarDelegate(
                        child: PreferredSize(
                      preferredSize: Size.fromHeight(40.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 8.0,
                            ),
                          ],
                        ),
                        child: Column(
                          children: <Widget>[
                            SafeArea(
                              child: Container(
                                width: 343,
                                height: 40,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  color: Color(0xffFAFAFA),
                                ),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.only(left: 8, right: 8),
                                  child: Center(
                                    child: TextField(
                                      decoration: InputDecoration(
                                          border: InputBorder.none,
                                          icon: Icon(
                                            Icons.search,
                                            size: 20,
                                          ),
                                          contentPadding: EdgeInsets.fromLTRB(
                                              0, 0.0, 20.0, 10.0),
                                          hintText:
                                              'Search merchan or promo here'),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
                  ),
                  SliverToBoxAdapter(
                    child: Column(
                      children: <Widget>[
                        PromoKeepedListPage(),
                        BannerListPage(),
                        FreeTreatsListPage(),
                        PromotionNearbyListPage(lat: widget.lat, long: widget.long),
                        CategoryListPage(),
                        SuperDealsListPage(lat: widget.lat, long: widget.long),
                        EventListPage(),
                        MerchantListPage(),
                        SizedBox(height: 70)
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  _SliverAppBarDelegate({this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return child;
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => 75;

  @override
  // TODO: implement minExtent
  double get minExtent => 75;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return false;
  }
}
