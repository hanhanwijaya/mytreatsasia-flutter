import 'package:asia/data/remote/models/response/Treats_Data_Remote/banner_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/free_treats_data_remote.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/foundation.dart';
import 'package:asia/data/remote/endpoints/endpoints.dart';
import 'package:asia/data/remote/models/response/team_list_data_remote.dart';
import 'package:rxdart/rxdart.dart';

class FreeTreatsListBloc extends BlocBase {
  final Endpoints endpoints;

  FreeTreatsListBloc(this.endpoints) {
    _getFreeTreatsListController.listen(_getFreeTreatsList);
  }

  final BehaviorSubject<FreeTreatsDataRemote> _freeTreatsListController =
  BehaviorSubject<FreeTreatsDataRemote>();

  Stream<FreeTreatsDataRemote> get freeTreatsList => _freeTreatsListController.stream;

  final BehaviorSubject<dynamic> _getFreeTreatsListController =
  BehaviorSubject<dynamic>();

  Sink<dynamic> get getFreeTreatsList => _getFreeTreatsListController.sink;

  void _getFreeTreatsList(dynamic) async {
    _freeTreatsListController.sink.add(await endpoints.getFreeTreatsList());
  }

  @override
  void dispose() {
    super.dispose();

    _freeTreatsListController.close();
    _getFreeTreatsListController.close();
  }

  @override
  bool get hasListeners => null;

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(VoidCallback listener) {}
}
