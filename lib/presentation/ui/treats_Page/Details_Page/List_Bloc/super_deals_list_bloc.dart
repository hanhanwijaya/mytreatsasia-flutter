import 'package:asia/data/remote/models/response/Treats_Data_Remote/banner_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/free_treats_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/promotion_nearby_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/super_deals_data_remote.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/foundation.dart';
import 'package:asia/data/remote/endpoints/endpoints.dart';
import 'package:asia/data/remote/models/response/team_list_data_remote.dart';
import 'package:rxdart/rxdart.dart';

class SuperDealsListBloc extends BlocBase {
  final Endpoints endpoints;

  SuperDealsListBloc(this.endpoints) {
    _getSuperDealsListController.listen(_getSuperDealsList);
  }

  final BehaviorSubject<SuperDealsDataRemote> _superDealsListController =
  BehaviorSubject<SuperDealsDataRemote>();

  Stream<SuperDealsDataRemote> get superDealsList => _superDealsListController.stream;

  final BehaviorSubject<String> _getSuperDealsListController =
  BehaviorSubject<String>();

  Sink<String> get getSuperDealsList => _getSuperDealsListController.sink;

  void _getSuperDealsList(String latLong) async {
    _superDealsListController.sink.add(await endpoints.getSuperDealsList(latLong));
  }

  @override
  void dispose() {
    super.dispose();

    _superDealsListController.close();
    _getSuperDealsListController.close();
  }

  @override
  bool get hasListeners => null;

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(VoidCallback listener) {}
}
