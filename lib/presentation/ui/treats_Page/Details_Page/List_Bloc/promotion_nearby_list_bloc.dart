import 'package:asia/data/remote/models/response/Treats_Data_Remote/banner_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/free_treats_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/promotion_nearby_data_remote.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/foundation.dart';
import 'package:asia/data/remote/endpoints/endpoints.dart';
import 'package:asia/data/remote/models/response/team_list_data_remote.dart';
import 'package:rxdart/rxdart.dart';

class PromotionNearbyListBloc extends BlocBase {
  final Endpoints endpoints;

  PromotionNearbyListBloc(this.endpoints) {
    _getPromotionNearbyListController.listen(_getPromotionNearbyList);
  }

  final BehaviorSubject<PromotionNearbyDataRemote> _promotionNearbyListController =
  BehaviorSubject<PromotionNearbyDataRemote>();

  Stream<PromotionNearbyDataRemote> get promotionNearbyList => _promotionNearbyListController.stream;

  final BehaviorSubject<String> _getPromotionNearbyListController =
  BehaviorSubject<String>();

  Sink<String> get getPromotionNearbyList => _getPromotionNearbyListController.sink;

  void _getPromotionNearbyList(String latLong) async {
    _promotionNearbyListController.sink.add(await endpoints.getPromotionNearbyList(latLong));
  }

  @override
  void dispose() {
    super.dispose();

    _promotionNearbyListController.close();
    _getPromotionNearbyListController.close();
  }

  @override
  bool get hasListeners => null;

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(VoidCallback listener) {}
}
