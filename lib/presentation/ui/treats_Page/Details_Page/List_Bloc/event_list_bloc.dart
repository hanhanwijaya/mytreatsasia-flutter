import 'package:asia/data/remote/models/response/Treats_Data_Remote/banner_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/category_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/event_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/free_treats_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/promotion_nearby_data_remote.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/foundation.dart';
import 'package:asia/data/remote/endpoints/endpoints.dart';
import 'package:asia/data/remote/models/response/team_list_data_remote.dart';
import 'package:rxdart/rxdart.dart';

class EventListBloc extends BlocBase {
  final Endpoints endpoints;

  EventListBloc(this.endpoints) {
    _getEventController.listen(_getEventList);
  }

  final BehaviorSubject<EventDataRemote> _eventListController =
  BehaviorSubject<EventDataRemote>();

  Stream<EventDataRemote> get eventList => _eventListController.stream;

  final BehaviorSubject<dynamic> _getEventController =
  BehaviorSubject<dynamic>();

  Sink<dynamic> get getEventList => _getEventController.sink;

  void _getEventList(dynamic) async {
    _eventListController.sink.add(await endpoints.getEventList());
  }

  @override
  void dispose() {
    super.dispose();

    _eventListController.close();
    _getEventController.close();
  }

  @override
  bool get hasListeners => null;

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(VoidCallback listener) {}
}
