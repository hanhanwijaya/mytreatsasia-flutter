import 'package:asia/data/remote/models/response/Treats_Data_Remote/banner_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/category_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/free_treats_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/promotion_nearby_data_remote.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/foundation.dart';
import 'package:asia/data/remote/endpoints/endpoints.dart';
import 'package:asia/data/remote/models/response/team_list_data_remote.dart';
import 'package:rxdart/rxdart.dart';

class CategoryListBloc extends BlocBase {
  final Endpoints endpoints;

  CategoryListBloc(this.endpoints) {
    _getCategoryListController.listen(_getCategoryList);
  }

  final BehaviorSubject<CategoryDataRemote> _categoryListController =
  BehaviorSubject<CategoryDataRemote>();

  Stream<CategoryDataRemote> get categoryList => _categoryListController.stream;

  final BehaviorSubject<dynamic> _getCategoryListController =
  BehaviorSubject<dynamic>();

  Sink<dynamic> get getCategoryList => _getCategoryListController.sink;

  void _getCategoryList(dynamic) async {
    _categoryListController.sink.add(await endpoints.getCategoryList());
  }

  @override
  void dispose() {
    super.dispose();

    _categoryListController.close();
    _getCategoryListController.close();
  }

  @override
  bool get hasListeners => null;

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(VoidCallback listener) {}
}
