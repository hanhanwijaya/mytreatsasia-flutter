import 'package:asia/data/remote/models/response/Treats_Data_Remote/banner_data_remote.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/foundation.dart';
import 'package:asia/data/remote/endpoints/endpoints.dart';
import 'package:asia/data/remote/models/response/team_list_data_remote.dart';
import 'package:rxdart/rxdart.dart';

class BannerListBloc extends BlocBase {
  final Endpoints endpoints;

  BannerListBloc(this.endpoints) {
    _getBannerListController.listen(_getBannerList);
  }

  final BehaviorSubject<BannerDataRemote> _bannerListController =
  BehaviorSubject<BannerDataRemote>();

  Stream<BannerDataRemote> get bannerList => _bannerListController.stream;

  final BehaviorSubject<dynamic> _getBannerListController =
  BehaviorSubject<dynamic>();

  Sink<dynamic> get getBannerList => _getBannerListController.sink;

  void _getBannerList(dynamic) async {
      _bannerListController.sink.add(await endpoints.getBannerList());
  }

  @override
  void dispose() {
    super.dispose();

    _bannerListController.close();
    _getBannerListController.close();
  }

  @override
  bool get hasListeners => null;

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(VoidCallback listener) {}
}
