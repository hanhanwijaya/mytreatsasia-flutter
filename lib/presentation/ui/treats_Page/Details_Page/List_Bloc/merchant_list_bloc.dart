import 'package:asia/data/remote/models/response/Treats_Data_Remote/banner_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/merchant_data_remote.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/foundation.dart';
import 'package:asia/data/remote/endpoints/endpoints.dart';
import 'package:asia/data/remote/models/response/team_list_data_remote.dart';
import 'package:rxdart/rxdart.dart';

class MerchantListBloc extends BlocBase {
  final Endpoints endpoints;

  MerchantListBloc(this.endpoints) {
    _getMerchantListController.listen(_getMerchantList);
  }

  final BehaviorSubject<MerchantDataRemote> _merchantListController =
  BehaviorSubject<MerchantDataRemote>();

  Stream<MerchantDataRemote> get merchantList => _merchantListController.stream;

  final BehaviorSubject<dynamic> _getMerchantListController =
  BehaviorSubject<dynamic>();

  Sink<dynamic> get getMerchantList => _getMerchantListController.sink;

  void _getMerchantList(dynamic) async {
    _merchantListController.sink.add(await endpoints.geMerchantList());
  }

  @override
  void dispose() {
    super.dispose();

    _merchantListController.close();
    _getMerchantListController.close();
  }

  @override
  bool get hasListeners => null;

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(VoidCallback listener) {}
}
