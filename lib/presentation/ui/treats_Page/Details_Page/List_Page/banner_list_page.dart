//import 'dart:html';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/banner_data_remote.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/banner_list_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';


class BannerListPage extends StatefulWidget {
  final String lat;
  final String long;

  BannerListPage({
    this.lat,
    this.long,
  });


  @override
  _BannerListPageState createState() => _BannerListPageState();
}

class _BannerListPageState extends State<BannerListPage> {
  final bannerListBloc = BlocProvider.getBloc<BannerListBloc>();

  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  int countPage = 0;

  void _animateSlider() {
    Future.delayed(Duration(seconds: 3)).then((_) {
      int nextPage = _pageController.page.round() + 1;

      if (nextPage == countPage) {
        nextPage = 0;
      }

      _pageController
          .animateToPage(nextPage, duration: Duration(seconds: 1), curve: Curves.linear)
          .then((_) => _animateSlider());
    });
  }

  @override
  void initState() {
    super.initState();
    bannerListBloc.getBannerList.add('');
    WidgetsBinding.instance.addPostFrameCallback((_) => _animateSlider());
//    print('latlong${widget.lat}\&${widget.long}');

  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<BannerDataRemote>(
            stream: bannerListBloc.bannerList,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              }
              if (snapshot.data != null) {
                countPage = snapshot.data.data.length;
                return Container(
                  height: 190.0,
                  child: Stack(
                    children: <Widget>[
                      PageView.builder(
                        controller: _pageController,
                        onPageChanged: (int page) {
                          _currentPage = page;
                          setState(() {});
                        },
                        itemCount: snapshot.data.data.length,
                        itemBuilder: (context, index){
                          return Padding(
                            padding: const EdgeInsets.only(left: 16, right: 16),
                            child: Column(
                              children: <Widget>[
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(8),
                                  child: Container(
                                    width: 343,
                                    height: 150,
                                    child: CachedNetworkImage(
                                      imageUrl: snapshot.data.data[index].picture, fit: BoxFit.cover,
                                      placeholder: (context, url) => Center(
                                          child: Container(
                                            width: 343,
                                            child: Shimmer.fromColors(
                                              baseColor: Color(0xffE5E5E6),
                                              highlightColor: Colors.white,
                                              direction: ShimmerDirection.ltr,
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(13),
                                                child: Container(
                                                  decoration: BoxDecoration(color: Color(0xffE5E5E6)),
                                                ),
                                              ),
                                            ),
                                          )
                                      ),
                                      errorWidget: (context, url, error) => Icon(Icons.error),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      Positioned(
                        top : 166.0,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Center(
                            child: SmoothPageIndicator(
                              controller: _pageController,
                              count: snapshot.data.data.length,
                              effect: WormEffect(),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Container(
//                child: Text('Error lurd!'),
              );
            });
  }
}
