import 'package:asia/data/remote/models/response/Treats_Data_Remote/category_data_remote.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/category_list_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CategoryListPage extends StatefulWidget {
  final String lat;
  final String long;

  CategoryListPage({
    this.lat,
    this.long,
  });

  @override
  _CategoryListPageState createState() => _CategoryListPageState();
}

class _CategoryListPageState extends State<CategoryListPage> {
  final categoryListBloc = BlocProvider.getBloc<CategoryListBloc>();

  @override
  void initState() {
    super.initState();
    categoryListBloc.getCategoryList.add('');
//    print('latlong${widget.lat}\&${widget.long}');
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CategoryDataRemote>(
        stream: categoryListBloc.categoryList,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
//            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.data != null) {
            return Container(
              child: Column(
                children: <Widget>[
//                    Category
                  Container(
                    height: 280.0,
                    width: 375.0,
                    decoration: BoxDecoration(color: Colors.transparent),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Category',
                            style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: 16.0,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            'Find your best fit for you',
                            style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: 12,
                                color: Color(0xff60666C)),
                          ),
                          Expanded(
                            child: GridView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3),
                              itemCount: snapshot.data.data.length,
                              itemBuilder: (context, index) {
                                var id =
                                    snapshot.data.data[index].id.toString();
                                return Container(
                                  height: 100.0,
                                  child: Card(
                                      elevation: 2,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          CachedNetworkImage(
                                            imageUrl:
                                                snapshot.data.data[index].icon,
                                            width: 50,
                                            height: 50,
                                            fit: BoxFit.cover,
                                            placeholder: (context, url) => Center(
                                                child:
                                                    CircularProgressIndicator()),
                                            errorWidget:
                                                (context, url, error) =>
                                                    Icon(Icons.error),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 12.0),
                                            child: Text(
                                              snapshot.data.data[index].name,
                                              style: TextStyle(
                                                  fontFamily: 'Rubik',
                                                  fontSize: 10),
                                            ),
                                          ),
                                        ],
                                      )),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          }
          return Container(
//            child: Text('Error lurd!'),
              );
        });
  }
}
