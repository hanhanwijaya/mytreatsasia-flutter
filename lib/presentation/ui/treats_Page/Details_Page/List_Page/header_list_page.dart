import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:page_transition/page_transition.dart';
import 'package:vibration/vibration.dart';

class HeaderListPage extends StatefulWidget {

  HeaderListPage({this.user, this.googleSignIn, this.name, this.lat, this.long});


  final FirebaseUser user;
  final GoogleSignIn googleSignIn;
  final name ;

  String lat;
  String long;


  @override
  _HeaderListPageState createState() => _HeaderListPageState();
}

class _HeaderListPageState extends State<HeaderListPage> {
  bool _isLoggedIn = false;



  String name;
  String pp;

  @override
  Widget build(BuildContext context) {

    final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = new GoogleSignIn();

    Future<FirebaseUser> _signIn() async {

      setState(() {
        _isLoggedIn = true;
      });

      GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
      GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.getCredential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);
      final AuthResult authResult =
      await firebaseAuth.signInWithCredential(credential);
      final FirebaseUser firebaseUser = authResult.user;

      name = firebaseUser.displayName;
      pp = firebaseUser.photoUrl;

//      Navigator.push(
//          context,
//          PageTransition(
//              type:
//              PageTransitionType
//                  .rightToLeft,
//              child: ProfileBasePage(user: firebaseUser, googleSignIn: googleSignIn,)));

      print('get id Token....${googleSignInAuthentication.idToken}');

      return firebaseUser;
    }



    if (name == null) {
      name = "";
      pp = "";
    } else {
      name = name;
      pp = pp;
    }

//    if (widget.user.displayName != null){
//      _isLoggedIn = false;
//    }
//    else{
//      _isLoggedIn = true;
//    }

    return Container(
      height: 130.0,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                  child: _isLoggedIn
                      ? Container(
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 16, right: 16, top: 16),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 40,
                              height: 40,
                              child: InkWell(
                                onTap: () {
                                  Vibration.vibrate(duration: 50);
                                  _signIn();

                                },
                                child: ClipOval(
                                  child: Container(
                                    decoration: BoxDecoration(
//                            color: Colors.red,
                                        image: DecorationImage(
                                            image: NetworkImage(pp))
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 16, bottom: 7),
                                  child: Text(
                                    'Hi, $name!',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontFamily: 'Rubik',
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black),
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                      const EdgeInsets.only(left: 16),
                                      child: Text(
                                        'MBR001191010',
                                        style: TextStyle(
                                            fontSize: 12.0,
                                            fontFamily: 'Rubik',
                                            color: Color(0xffA0A4A8)),
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                      const EdgeInsets.only(left: 4),
                                      child: ClipOval(
                                        child: Container(
                                          width: 10,
                                          height: 10,
                                          decoration: BoxDecoration(
                                              color: Color(0xff855CD6)),
                                          child: Center(
                                              child: Icon(
                                                Icons.done,
                                                color: Colors.white,
                                                size: 8,
                                              )),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                      : Container(
                    height: 50,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16, left: 16),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                            width: 100,
                            child: RaisedButton(
                              onPressed: () {
                                _signIn();
                              },
                              padding: const EdgeInsets.all(0.0),
                              child: Ink(
                                decoration: const BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.bottomLeft,
                                      end: Alignment.topRight,
                                      stops: [
                                        0.2,
                                        1,
                                      ],
                                      colors: [
                                        Color(0xff563399),
                                        Color(0xff9E85BF),
                                      ]),
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(8.0)),
                                ),
                                child: Container(
                                  constraints: const BoxConstraints(
                                      minWidth: 88.0, minHeight: 36.0),
                                  // min sizes for Material buttons
                                  alignment: Alignment.center,
                                  child: const Text(
                                    'Log in',
                                    style: TextStyle(color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            )),
                      ),
                    ),
                  )),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 16, top: 16),
                  child: Align(
                      alignment: Alignment.topRight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          MaterialButton(
                              minWidth: 10,
//                              onPressed: (){
//                                Vibration.vibrate(duration: 50);
//                                Navigator.push(
//                                    context,
//                                    PageTransition(
//                                        type:
//                                        PageTransitionType
//                                            .fade,
//                                        child: NearbyListPage(
//                                            lat: widget.lat.toString(),
//                                            long: widget.long.toString()
//                                        )));
////                                NearbyListPage(
////                                    lat: widget.lat.toString(),
////                                    long: widget.long.toString());
//                              },
                              child: Container(
                                  width: 24,
                                  height: 24,
                                  child: SvgPicture.asset('images/header/ic_headerMaps.svg'))),
                          Padding(
                            padding: const EdgeInsets.only(left: 6),
                            child: InkWell(
//                                onTap: (){
//                                  Navigator.push(
//                                      context,
//                                      PageTransition(
//                                        type: PageTransitionType.rightToLeft,
//                                        child: QrPage(),
//                                      ));
//                                },
                                child: Container(child: SvgPicture.asset('images/header/ic_Qr.svg'))),
                          ),
                        ],
                      )),
                ),
              ),
            ],
          ),

//          Padding(
//            padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
//            child: ClipRRect(
//              borderRadius: BorderRadius.circular(8),
//              child: Container(
//                decoration: BoxDecoration(color: Color(0xffFAFAFA)),
//                child: Padding(
//                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
//                  child: Container(
//                    child: TextField(
//                      decoration: InputDecoration(
//                          border: InputBorder.none,
//                          icon: Icon(Icons.search),
//                          hintText: 'Search merchan or promo here'),
//                    ),
//                  ),
//                ),
//              ),
//            ),
//          ),
        ],
      ),
    );
  }
}

//140952