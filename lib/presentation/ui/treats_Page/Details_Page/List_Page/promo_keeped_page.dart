import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PromoKeepedListPage extends StatefulWidget {
  @override
  _PromoKeepedListPageState createState() => _PromoKeepedListPageState();
}

class _PromoKeepedListPageState extends State<PromoKeepedListPage> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Ink(
        child: Container(
          height: 80.0,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            boxShadow: [BoxShadow(
              color: Colors.grey,
              blurRadius: 5.0,
            ),],
            gradient: LinearGradient(
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
                stops: [
                  0.2,
                  1,
                ],
                colors: [
                  Color(0xff563399),
                  Color(0xff9E85BF),
                ]),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 16, left: 16, right: 16, bottom: 4),
                        child: SvgPicture.asset('images/body/ic_Promo.svg'),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Text(
                              'Promo Keeped',
                              style:
                              TextStyle(fontSize: 12, fontFamily: 'Rubik', color: Color.fromRGBO(255,255,255,0.5)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 4),
                            child: Text(
                              '30 Promos',
                              style:
                              TextStyle(fontSize: 20, fontFamily: 'Rubik', fontWeight: FontWeight.w500, color: Color(0xffffffff)),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: IconButton(
                        icon: Icon(
                          Icons.arrow_forward_ios,
                          color: Color(0xffffffff),
                        ),
                        onPressed: () {}),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
