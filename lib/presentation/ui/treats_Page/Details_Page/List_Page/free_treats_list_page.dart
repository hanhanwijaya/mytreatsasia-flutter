import 'package:asia/data/remote/models/response/Treats_Data_Remote/free_treats_data_remote.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/free_treats_list_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';


class FreeTreatsListPage extends StatefulWidget {
  final String lat;
  final String long;

  FreeTreatsListPage({
    this.lat,
    this.long,
  });


  @override
  _FreeTreatsListPageState createState() => _FreeTreatsListPageState();
}

class _FreeTreatsListPageState extends State<FreeTreatsListPage> {
  final freeTreatsListBloc = BlocProvider.getBloc<FreeTreatsListBloc>();


  @override
  void initState() {
    super.initState();
    freeTreatsListBloc.getFreeTreatsList.add('');
//    print('latlong${widget.lat}\&${widget.long}');
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<FreeTreatsDataRemote>(
        stream: freeTreatsListBloc.freeTreatsList,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
//            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.data != null) {
            return Container(
                child: snapshot.data.data.length > 0
                    ? Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 239.0,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 3.0,
                            ),
                          ],
                          color: Colors.white,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                              const EdgeInsets.only(left: 16.0, top: 16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Free Promo\'s For You',
                                    style: TextStyle(
                                        fontFamily: 'Rubik',
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Text(
                                    'you have ${snapshot.data.data.length} more left. Grab it fast!',
                                    style: TextStyle(
                                        fontFamily: 'Rubik',
                                        fontSize: 12.0,
                                        color: Color(0xff60666C)),
                                  ),
                                  Container(
                                      child: new Container(
                                          margin:
                                          EdgeInsets.symmetric(vertical: 20.0),
                                          height: 150.0,
                                          child: ListView.builder(
                                              physics: BouncingScrollPhysics(),
                                              scrollDirection: Axis.horizontal,
                                              itemCount: snapshot.data.data.length,
                                              itemBuilder: (context, index) {
                                                return Padding(
                                                  padding: const EdgeInsets.only(
                                                      right: 16.0),
                                                  child: Ink(
                                                    color: Colors.blue,
                                                    child: Container(
                                                      width: 253.0,
                                                      child: ClipRRect(
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                        child: CachedNetworkImage(
                                                          imageUrl: snapshot
                                                              .data
                                                              .data[index]
                                                              .picture,
                                                          fit: BoxFit.cover,
                                                          placeholder:
                                                              (context, url) =>
                                                              Container(
                                                                width: 253,
                                                                child: ClipRRect(
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                      8),
                                                                  child: Shimmer
                                                                      .fromColors(
                                                                    baseColor: Color(
                                                                        0xffE5E5E6),
                                                                    highlightColor:
                                                                    Colors.white,
                                                                    direction:
                                                                    ShimmerDirection
                                                                        .ltr,
                                                                    child: ClipRRect(
                                                                      borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                          13),
                                                                      child:
                                                                      Container(
                                                                        decoration:
                                                                        BoxDecoration(
                                                                            color:
                                                                            Color(0xffE5E5E6)),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                          errorWidget: (context,
                                                              url, error) =>
                                                              Icon(Icons.error),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              }))
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
                    : Container()
            );
          }
          return Container(
//            child: Text('Error lurd!'),
          );
        });
  }
}
