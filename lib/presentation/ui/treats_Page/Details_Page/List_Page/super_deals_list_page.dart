import 'package:asia/data/remote/models/response/Treats_Data_Remote/super_deals_data_remote.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/super_deals_list_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class SuperDealsListPage extends StatefulWidget {
  final String lat;
  final String long;

  SuperDealsListPage({
    this.lat,
    this.long,
  });

  @override
  _SuperDealsListPageState createState() => _SuperDealsListPageState();
}

class _SuperDealsListPageState extends State<SuperDealsListPage> {
  final superDealsListBloc = BlocProvider.getBloc<SuperDealsListBloc>();

  @override
  void initState() {
    super.initState();
    String laLong = ('${widget.lat}\,${widget.long}');
    superDealsListBloc.getSuperDealsList.add(laLong);
    print('latlong${widget.lat}\&${widget.long}');
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<SuperDealsDataRemote>(
        stream: superDealsListBloc.superDealsList,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
//            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.data != null) {
            return Container(
                child: snapshot.data.data.length > 0
                    ? Container(
                        child: Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: Container(
                          height: 288.0,
                          width: 375.0,
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 3.0,
                              ),
                            ],
                            gradient: LinearGradient(
                                begin: Alignment.bottomLeft,
                                end: Alignment.topRight,
                                stops: [
                                  0.2,
                                  1,
                                ],
                                colors: [
                                  Color(0xffE64C4D),
                                  Color(0xfff5bcbc),
                                ]),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 16.0, right: 16.0, top: 24.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Super DealsNearby',
                                      style: TextStyle(
                                          fontFamily: 'Rubik',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                    ),
                                    Text(
                                      'See More',
                                      style: TextStyle(
                                          fontFamily: 'Rubik',
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                  child: new Padding(
                                padding: const EdgeInsets.only(
                                  top: 16.0,
                                ),
                                child: (Container(
                                  height: 207.0,
                                  child: CarouselSlider.builder(
                                      height: 207.0,
                                      aspectRatio: 2,
                                      viewportFraction: 0.9,
                                      initialPage: 0,
                                      enableInfiniteScroll: true,
                                      autoPlay: true,
                                      autoPlayInterval: Duration(seconds: 5),
                                      autoPlayAnimationDuration:
                                          Duration(milliseconds: 1000),
                                      pauseAutoPlayOnTouch:
                                          Duration(seconds: 5),
                                      enlargeCenterPage: true,
                                      scrollDirection: Axis.horizontal,
                                      itemCount: snapshot.data.data.length,
                                      itemBuilder: (context, index) {
                                        return Builder(
                                          builder: (BuildContext context) {
                                            return Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Expanded(
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            13),
                                                    child: Container(
                                                      width: 400.0,
                                                      margin:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 5.0),
                                                      child: CachedNetworkImage(
                                                        imageUrl: snapshot
                                                            .data
                                                            .data[index]
                                                            .image[0]
                                                            .file,
                                                        fit: BoxFit.cover,
                                                        placeholder:
                                                            (context, url) =>
                                                                Container(
                                                          width: 400,
                                                          child: ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        13),
                                                            child: Shimmer
                                                                .fromColors(
                                                              baseColor: Color(
                                                                  0xffE5E5E6),
                                                              highlightColor:
                                                                  Colors.white,
                                                              direction:
                                                                  ShimmerDirection
                                                                      .ltr,
                                                              child: ClipRRect(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            13),
                                                                child:
                                                                    Container(
                                                                  decoration:
                                                                      BoxDecoration(
                                                                          color:
                                                                              Color(0xffE5E5E6)),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Icon(Icons.error),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 10, right: 10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Text(
                                                        snapshot
                                                            .data
                                                            .data[index]
                                                            .category,
                                                        style: TextStyle(
                                                            fontFamily: 'Rubik',
                                                            fontSize: 16,
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      ),
                                                      Column(
                                                        children: <Widget>[
                                                          Text(
                                                            '\RM ${snapshot.data.data[index].originalRetailPrice}\.00',
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    'Rubik',
                                                                fontSize: 12,
                                                                color: Color
                                                                    .fromRGBO(
                                                                        255,
                                                                        255,
                                                                        255,
                                                                        0.5),
                                                                decoration:
                                                                    TextDecoration
                                                                        .lineThrough),
                                                          ),
                                                          Text(
                                                            '\RM ${snapshot.data.data[index].originalRetailPrice}\.00',
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    'Rubik',
                                                                fontSize: 20,
                                                                color: Color(
                                                                    0xffffffff)),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            );
                                          },
                                        );
                                      }),
                                )),
                              ))
                            ],
                          ),
                        ),
                      ))
                    : Container());
          }
          return Container(
//            child: Text('Error lurd!'),
              );
        });
  }
}
