import 'package:asia/data/remote/models/response/Treats_Data_Remote/merchant_data_remote.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/merchant_list_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class MerchantListPage extends StatefulWidget {
  final String lat;
  final String long;

  MerchantListPage({
    this.lat,
    this.long,
  });

  @override
  _MerchantListPageState createState() => _MerchantListPageState();
}

class _MerchantListPageState extends State<MerchantListPage> {
  final merchantListBloc = BlocProvider.getBloc<MerchantListBloc>();

  @override
  void initState() {
    super.initState();
    merchantListBloc.getMerchantList.add('');
    print('latlong${widget.lat}\&${widget.long}');
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MerchantDataRemote>(
        stream: merchantListBloc.merchantList,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
//            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.data != null) {
            return Container(
              height: 150.0,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 3.0,
                  ),
                ],
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 16.0, right: 16.0, top: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Official Merchant',
                          style: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff303336)),
                        ),
                        Text(
                          'See More',
                          style: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: 12.0,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff60666C)),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 13.0,
                      left: 13,
                    ),
                    child: (Container(
                      height: 100,
                      width: 375.0,
                      child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.data.length,
                        itemBuilder: (context, index) {
                          return Container(
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 16.0),
                                      child: Column(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 3.0, left: 3.0),
                                            child: Container(
                                              width: 64.0,
                                              height: 64.0,
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              // borde width
                                              decoration: new BoxDecoration(
                                                color: const Color(0xFFFFFFFF),
                                                // border color
                                                shape: BoxShape.circle,
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.grey,
                                                    blurRadius: 3.0,
                                                  ),
                                                ],
                                              ),
                                              child: Center(
                                                child: Container(
                                                  width: 58,
                                                  height: 58,
                                                  decoration: new BoxDecoration(
                                                    color: Colors.white,
                                                    // border color
                                                    shape: BoxShape.circle,
                                                  ),
                                                  child: ClipOval(
                                                    child: CachedNetworkImage(
                                                      imageUrl: snapshot.data
                                                          .data[index].avatar,
                                                      fit: BoxFit.cover,
                                                      placeholder: (context,
                                                              url) =>
                                                          Center(
                                                              child: Container(
                                                        width: 58,
                                                        height: 58,
                                                        child:
                                                            Shimmer.fromColors(
                                                          baseColor:
                                                              Color(0xffE5E5E6),
                                                          highlightColor:
                                                              Colors.white,
                                                          direction:
                                                              ShimmerDirection
                                                                  .ltr,
                                                          child: ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        13),
                                                            child: Container(
                                                              decoration: BoxDecoration(
                                                                  shape: BoxShape
                                                                      .circle,
                                                                  color: Color(
                                                                      0xffE5E5E6)),
                                                            ),
                                                          ),
                                                        ),
                                                      )),
                                                      errorWidget: (context,
                                                              url, error) =>
                                                          Icon(Icons.error),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 16.0),
                                            child: Container(
                                              width: 60,
                                              child: Container(
                                                child: Center(
                                                  child: Text(
                                                    snapshot
                                                        .data.data[index].name,
                                                    style: TextStyle(
                                                        fontSize: 12.0,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    softWrap: true,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    )),
                  ),
                ],
              ),
            );
          }
          return Container(
//            child: Text('Error lurd!'),
              );
        });
  }
}
