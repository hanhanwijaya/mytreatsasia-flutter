import 'package:asia/data/remote/models/response/Treats_Data_Remote/promotion_nearby_data_remote.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/promotion_nearby_list_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class PromotionNearbyListPage extends StatefulWidget {
  final String lat;
  final String long;

  PromotionNearbyListPage({
    this.lat,
    this.long,
  });

  @override
  _PromotionNearbyListPageState createState() =>
      _PromotionNearbyListPageState();
}

class _PromotionNearbyListPageState extends State<PromotionNearbyListPage> {
  final promotionNearbyListBloc =
      BlocProvider.getBloc<PromotionNearbyListBloc>();

  @override
  void initState() {
    super.initState();
    String laLong = ('${widget.lat}\,${widget.long}');
    promotionNearbyListBloc.getPromotionNearbyList.add(laLong);
    print('latlongpromotion${widget.lat}\&${widget.long}');
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PromotionNearbyDataRemote>(
        stream: promotionNearbyListBloc.promotionNearbyList,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
//            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.data != null) {
            return Container(
                child: snapshot.data.data.length > 0
                    ? Container(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 16.0, bottom: 16),
                          child: (Container(
                            height: 320.0,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 3.0,
                                ),
                              ],
                              color: Colors.white,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 16.0, top: 16.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            'F&B Promotions Nearby',
                                            style: TextStyle(
                                                fontFamily: 'Rubik',
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 16),
                                            child: Text(
                                              'See More',
                                              style: TextStyle(
                                                  fontFamily: 'Rubik',
                                                  fontSize: 12.0,
                                                  color: Color(0xff60666C)),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Text(
                                        'Try these and thanks us later !',
                                        style: TextStyle(
                                            fontFamily: 'Rubik',
                                            fontSize: 12.0,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.grey),
                                      ),
                                      Container(
                                          child: new Container(
                                        height: 255.0,
                                        margin:
                                            EdgeInsets.symmetric(vertical: 5.0),
                                        child: ListView.builder(
                                          physics: BouncingScrollPhysics(),
                                          scrollDirection: Axis.horizontal,
                                          itemCount: snapshot.data.data.length,
                                          itemBuilder: (context, index) {
                                            var price = (snapshot
                                                    .data.data[index].price -
                                                snapshot.data.data[index]
                                                    .limitation);
                                            return ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              child: Card(
                                                elevation: 2,
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topLeft: Radius
                                                                  .circular(8),
                                                              topRight: Radius
                                                                  .circular(8)),
                                                      child: CachedNetworkImage(
                                                        imageUrl: snapshot.data
                                                            .data[index].image,
                                                        width: 213,
                                                        height: 150,
                                                        fit: BoxFit.cover,
                                                        placeholder:
                                                            (context, url) =>
                                                                Container(
                                                          height: 153,
                                                          width: 213,
                                                          child: Shimmer
                                                              .fromColors(
                                                            baseColor: Color(
                                                                0xffE5E5E6),
                                                            highlightColor:
                                                                Colors.white,
                                                            direction:
                                                                ShimmerDirection
                                                                    .ltr,
                                                            child: ClipRRect(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          13),
                                                              child: Container(
                                                                decoration: BoxDecoration(
                                                                    color: Color(
                                                                        0xffE5E5E6)),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            Icon(Icons.error),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        8),
                                                                bottomRight:
                                                                    Radius
                                                                        .circular(
                                                                            8)),
                                                        child: Container(
                                                          width: 213,
                                                          decoration:
                                                              BoxDecoration(
                                                                  color: Colors
                                                                      .white),
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 12.0,
                                                                    right: 12.0,
                                                                    top: 12.0),
                                                            child: Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  snapshot
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .title,
                                                                  style: TextStyle(
                                                                      fontFamily:
                                                                          'Rubik',
                                                                      fontSize:
                                                                          12.0,
                                                                      color: Color(
                                                                          0xff303336),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500),
                                                                ),
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      top:
                                                                          12.0),
                                                                  child: Row(
                                                                    children: <
                                                                        Widget>[
                                                                      RichText(
                                                                          text: TextSpan(
                                                                              style: TextStyle(fontFamily: 'Rubik', fontSize: 16, color: Color(0xff303336), decoration: TextDecoration.lineThrough),
                                                                              children: [
                                                                            TextSpan(text: 'RM ${snapshot.data.data[index].price}'),
                                                                            TextSpan(
                                                                                text: '.00',
                                                                                style: TextStyle(fontSize: 12)),
                                                                          ])),
                                                                      Padding(
                                                                        padding:
                                                                            const EdgeInsets.only(left: 8),
                                                                        child: RichText(
                                                                            text: TextSpan(style: TextStyle(fontFamily: 'Rubik', fontSize: 20, color: Color(0xffffEB4747), fontWeight: FontWeight.bold), children: [
                                                                          TextSpan(
                                                                              text: 'RM $price'),
                                                                          TextSpan(
                                                                              text: '.00',
                                                                              style: TextStyle(fontSize: 16)),
                                                                        ])),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      top:
                                                                          12.0),
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    children: <
                                                                        Widget>[
                                                                      Container(
                                                                        child:
                                                                            CircleAvatar(
                                                                          backgroundImage: NetworkImage(snapshot
                                                                              .data
                                                                              .data[index]
                                                                              .partnerAvatar),
                                                                        ),
                                                                        width:
                                                                            16.0,
                                                                        height:
                                                                            16.0,
                                                                      ),
                                                                      Padding(
                                                                        padding:
                                                                            const EdgeInsets.only(left: 8.0),
                                                                        child:
                                                                            Text(
                                                                          snapshot
                                                                              .data
                                                                              .data[index]
                                                                              .partnerName,
                                                                          style: TextStyle(
                                                                              fontFamily: 'Rubik',
                                                                              fontSize: 10),
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      ))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )),
                        ),
                      )
                    : Container());
          }
          return Container(
//            child: Text('Error lurd!'),
              );
        });
  }
}
