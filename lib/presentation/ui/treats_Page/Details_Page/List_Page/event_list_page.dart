import 'package:asia/data/remote/models/response/Treats_Data_Remote/event_data_remote.dart';
import 'package:asia/presentation/ui/treats_Page/Details_Page/List_Bloc/event_list_bloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shimmer/shimmer.dart';

class EventListPage extends StatefulWidget {
  final String lat;
  final String long;

  EventListPage({
    this.lat,
    this.long,
  });

  @override
  _EventListPageState createState() => _EventListPageState();
}

class _EventListPageState extends State<EventListPage> {
  final eventListBloc = BlocProvider.getBloc<EventListBloc>();

  @override
  void initState() {
    super.initState();
    eventListBloc.getEventList.add('');
    print('latlong${widget.lat}\&${widget.long}');
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<EventDataRemote>(
        stream: eventListBloc.eventList,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
//            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.data != null) {
            return Container(
                child: snapshot.data.data.length > 0
                    ? Padding(
                        padding: const EdgeInsets.only(
                          top: 24.0,
                          bottom: 24.0,
                        ),
                        child: Container(
                          height: 221.0,
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                          ),
                          child: ListView.builder(
                            physics: BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.data.length + 1,
                            itemBuilder: (context, index) {
                              if (index == 0) {
                                return Card(
                                  elevation: 0,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        child: SvgPicture.asset(
                                          'images/body/header_event.svg',
                                          width: 140,
                                          height: 71,
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 16),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Popular Event',
                                                style: TextStyle(
                                                    fontFamily: 'Rubik',
                                                    fontSize: 16.0,
                                                    color: Color(0xff303336),
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                              SizedBox(
                                                height: 8.0,
                                              ),
                                              Container(
                                                width: 147,
                                                child: Text(
                                                  'Find the nearest Event here and get special prize from MyTreats',
                                                  style: TextStyle(
                                                      fontFamily: 'Rubik',
                                                      fontSize: 12.0,
                                                      color: Color(0xff303336),
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      height: 1.2),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 12,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                          width: 150,
                                          child: new OutlineButton(
                                              child: new Text(
                                                "View All Events",
                                                style: TextStyle(
                                                    fontFamily: 'Rubik',
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w500,
                                                    color: Color(0xff855CD6)),
                                              ),
                                              onPressed: null,
                                              disabledBorderColor:
                                                  Color(0xff855CD6),
                                              shape: new RoundedRectangleBorder(
                                                  borderRadius:
                                                      new BorderRadius.circular(
                                                          8.0))))
                                    ],
                                  ),
                                );
                              } else {
//                      index = index-1;
                                return ClipRRect(
                                  borderRadius: BorderRadius.circular(16),
                                  child: Card(
                                    elevation: 2,
                                    child: Container(
                                      width: 151,
                                      height: 221,
                                      child: Stack(
                                        children: <Widget>[
                                          Container(
                                            width: 151,
                                            height: 221,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              child: CachedNetworkImage(
                                                imageUrl: snapshot.data
                                                    .data[index - 1].picture,
                                                fit: BoxFit.cover,
                                                placeholder: (context, url) =>
                                                    Center(
                                                        child: Container(
                                                  width: 151,
                                                  height: 221,
                                                  child: Shimmer.fromColors(
                                                    baseColor:
                                                        Color(0xffE5E5E6),
                                                    highlightColor:
                                                        Colors.white,
                                                    direction:
                                                        ShimmerDirection.ltr,
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              13),
                                                      child: Container(
                                                        decoration:
                                                            BoxDecoration(
                                                                color: Color(
                                                                    0xffE5E5E6)),
                                                      ),
                                                    ),
                                                  ),
                                                )),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Icon(Icons.error),
                                              ),
                                            ),
                                          ),
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            child: Container(
                                              width: 151,
                                              height: 221,
                                              decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                    begin:
                                                        Alignment.bottomCenter,
                                                    end: Alignment.topCenter,
                                                    stops: [
                                                      0.2,
                                                      1,
                                                    ],
                                                    colors: [
                                                      Color.fromRGBO(
                                                          86, 51, 153, 0.8),
                                                      Color.fromRGBO(
                                                          86, 51, 153, 0.05),
                                                    ]),
                                              ),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            bottom: 16.0),
                                                    child: Text(
                                                      snapshot
                                                          .data
                                                          .data[index - 1]
                                                          .title,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      )
                    : Container());
          }
          return Container(
//            child: Text('Error lurd!'),
              );
        });
  }
}
