import 'package:asia/data/remote/models/response/Treats_Data_Remote/banner_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/category_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/event_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/free_treats_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/merchant_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/promotion_nearby_data_remote.dart';
import 'package:asia/data/remote/models/response/Treats_Data_Remote/super_deals_data_remote.dart';
import 'package:dio/dio.dart';
import 'package:asia/constants.dart';
import 'package:asia/data/remote/models/response/team_detail_data_remote.dart';
import 'package:asia/data/remote/models/response/team_list_data_remote.dart';
import 'package:asia/di/network_dependencies.dart';


const banner = "member/banner/list?page=1&fl_frontend=&on_period=1";
const freeTreats = "member/free_treats/list";
const promotionNearby = "member/promo_regular/nearby";
const category = "member/home/category";
const superDeals = "member/superdeal/list";
const event = "member/event/list";
const merchant = "member/merchant/list?page=&category_id&country_id&district&subdistrict&subcategory&search=";

const teamList = "search_all_teams.php?l";
const teamDetail = "lookupteam.php?id";

class Endpoints {
  Dio _dio;

  Endpoints() {
    _dio = Dio();

////MASTER ==============================
//    _dio.options.baseUrl = "http://api.mytreats.asia/v1/";
////====================================

//Dev ==============================
    _dio.options.baseUrl = "http://devmyt.exaditama.id/v1/";
//====================================


    _dio.options.connectTimeout = connectionTimeout;
    _dio.options.receiveTimeout = connectionReadTimeout;

    var loggingInterceptor = getLoggingInterceptor();
    var errorInterceptor = getErrorInterceptor(loggingInterceptor);
    var responseInterceptor = getResponseInterceptor(loggingInterceptor);
    var requestInterceptor = getRequestInterceptor(loggingInterceptor);

    _dio.interceptors.add(InterceptorsWrapper(
        onRequest: (RequestOptions options) async =>
            await requestInterceptor.getRequestInterceptor(options),
        onResponse: (Response response) =>
            responseInterceptor.getResponseInterceptor(response),
        onError: (DioError dioError) =>
            errorInterceptor.getErrorInterceptors(dioError)));
  }

  Future<BannerDataRemote> getBannerList() async {
    Response response = await _dio.get(banner);
    return BannerDataRemote.fromJson(response.data);
  }

  Future<FreeTreatsDataRemote> getFreeTreatsList() async {
    Response response = await _dio.get(freeTreats);
    return FreeTreatsDataRemote.fromJson(response.data);
  }

  Future<PromotionNearbyDataRemote> getPromotionNearbyList(String latLong) async {
    var getlatLong = latLong.split(",");
    String lat = getlatLong[0];
    String long = getlatLong[1];
    
    Response response = await _dio.post(promotionNearby, data: {
      'latitude': lat,
      'longitude': long,
      'page': '1',
      'max_distance': '300000000',
      'category_id': '7',
      'promo_type': '1',
      'search': ''
      }
    );
    return PromotionNearbyDataRemote.fromJson(response.data);
  }

  Future<SuperDealsDataRemote> getSuperDealsList(String latLong) async {
    var getlatLong = latLong.split(",");
    String lat = getlatLong[0];
    String long = getlatLong[1];

    Response response = await _dio.post(superDeals,data: {
      'latitude': lat,
      'longitude': long,
      'page': '',
      'max_distance': '300000000',
      'category_id': '',
      'search': ''
      }
    );
    return SuperDealsDataRemote.fromJson(response.data);
  }


  Future<CategoryDataRemote> getCategoryList() async {
    Response response = await _dio.get(category);
    return CategoryDataRemote.fromJson(response.data);
  }

  Future<EventDataRemote> getEventList() async {
    Response response = await _dio.get(event);
    return EventDataRemote.fromJson(response.data);
  }

  Future<MerchantDataRemote> geMerchantList() async {
    Response response = await _dio.get(merchant);
    return MerchantDataRemote.fromJson(response.data);
  }

  Future<TeamListDataRemote> getListTeam(String leagueName) async {
    Response response = await _dio.get("$teamList=$leagueName");
    return TeamListDataRemote.fromJson(response.data);
  }

  Future<TeamDetailDataRemote> getDetailTeam(String teamId) async {
    Response response = await _dio.get("$teamDetail=$teamId");
    return TeamDetailDataRemote.fromJson(response.data);
  }
}
