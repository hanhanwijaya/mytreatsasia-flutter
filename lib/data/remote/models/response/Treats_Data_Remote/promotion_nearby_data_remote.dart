// To parse this JSON data, do
//
//     final promotionNearbyDataRemote = promotionNearbyDataRemoteFromJson(jsonString);

import 'dart:convert';

PromotionNearbyDataRemote promotionNearbyDataRemoteFromJson(String str) => PromotionNearbyDataRemote.fromJson(json.decode(str));

String promotionNearbyDataRemoteToJson(PromotionNearbyDataRemote data) => json.encode(data.toJson());

class PromotionNearbyDataRemote {
  bool status;
  String message;
  List<Datum> data;
  Meta meta;
  List<dynamic> error;

  PromotionNearbyDataRemote({
    this.status,
    this.message,
    this.data,
    this.meta,
    this.error,
  });

  factory PromotionNearbyDataRemote.fromJson(Map<String, dynamic> json) => PromotionNearbyDataRemote(
    status: json["status"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    meta: Meta.fromJson(json["meta"]),
    error: List<dynamic>.from(json["error"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "meta": meta.toJson(),
    "error": List<dynamic>.from(error.map((x) => x)),
  };
}

class Datum {
  int id;
  String title;
  String shortDesc;
  String image;
  int price;
  int limitation;
  List<Category> category;
  int muslimFriendly;
  DateTime startDate;
  DateTime endDate;
  String startHour;
  String endHour;
  bool favorite;
  String status;
  String type;
  String seasonalType;
  dynamic seasonalId;
  dynamic seasonalName;
  String promotionType;
  String qrCode;
  int partnerId;
  String partnerName;
  String partnerSignature;
  String partnerAvatar;
  String partnerAddress;
  String partnerLatitude;
  String partnerLongitude;
  double partnerRating;
  double partnerRatingOrder;
  double partnerRatingProduct;
  double partnerRatingPricing;
  double partnerRatingFriendliness;
  int partnerFeedback;
  List<PartnerOfficeHour> partnerOfficeHours;
  String isBarcode;
  dynamic barcode;

  Datum({
    this.id,
    this.title,
    this.shortDesc,
    this.image,
    this.price,
    this.limitation,
    this.category,
    this.muslimFriendly,
    this.startDate,
    this.endDate,
    this.startHour,
    this.endHour,
    this.favorite,
    this.status,
    this.type,
    this.seasonalType,
    this.seasonalId,
    this.seasonalName,
    this.promotionType,
    this.qrCode,
    this.partnerId,
    this.partnerName,
    this.partnerSignature,
    this.partnerAvatar,
    this.partnerAddress,
    this.partnerLatitude,
    this.partnerLongitude,
    this.partnerRating,
    this.partnerRatingOrder,
    this.partnerRatingProduct,
    this.partnerRatingPricing,
    this.partnerRatingFriendliness,
    this.partnerFeedback,
    this.partnerOfficeHours,
    this.isBarcode,
    this.barcode,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    title: json["title"],
    shortDesc: json["short_desc"],
    image: json["image"],
    price: json["price"],
    limitation: json["limitation"],
    category: List<Category>.from(json["category"].map((x) => Category.fromJson(x))),
    muslimFriendly: json["muslim_friendly"],
    startDate: DateTime.parse(json["start_date"]),
    endDate: DateTime.parse(json["end_date"]),
    startHour: json["start_hour"],
    endHour: json["end_hour"],
    favorite: json["favorite"],
    status: json["status"],
    type: json["type"],
    seasonalType: json["seasonal_type"],
    seasonalId: json["seasonal_id"],
    seasonalName: json["seasonal_name"],
    promotionType: json["promotion_type"],
    qrCode: json["qr_code"],
    partnerId: json["partner_id"],
    partnerName: json["partner_name"],
    partnerSignature: json["partner_signature"],
    partnerAvatar: json["partner_avatar"],
    partnerAddress: json["partner_address"],
    partnerLatitude: json["partner_latitude"],
    partnerLongitude: json["partner_longitude"],
    partnerRating: json["partner_rating"].toDouble(),
    partnerRatingOrder: json["partner_rating_order"].toDouble(),
    partnerRatingProduct: json["partner_rating_product"].toDouble(),
    partnerRatingPricing: json["partner_rating_pricing"].toDouble(),
    partnerRatingFriendliness: json["partner_rating_friendliness"].toDouble(),
    partnerFeedback: json["partner_feedback"],
    partnerOfficeHours: List<PartnerOfficeHour>.from(json["partner_office_hours"].map((x) => PartnerOfficeHour.fromJson(x))),
    isBarcode: json["is_barcode"],
    barcode: json["barcode"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "short_desc": shortDesc,
    "image": image,
    "price": price,
    "limitation": limitation,
    "category": List<dynamic>.from(category.map((x) => x.toJson())),
    "muslim_friendly": muslimFriendly,
    "start_date": "${startDate.year.toString().padLeft(4, '0')}-${startDate.month.toString().padLeft(2, '0')}-${startDate.day.toString().padLeft(2, '0')}",
    "end_date": "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
    "start_hour": startHour,
    "end_hour": endHour,
    "favorite": favorite,
    "status": status,
    "type": type,
    "seasonal_type": seasonalType,
    "seasonal_id": seasonalId,
    "seasonal_name": seasonalName,
    "promotion_type": promotionType,
    "qr_code": qrCode,
    "partner_id": partnerId,
    "partner_name": partnerName,
    "partner_signature": partnerSignature,
    "partner_avatar": partnerAvatar,
    "partner_address": partnerAddress,
    "partner_latitude": partnerLatitude,
    "partner_longitude": partnerLongitude,
    "partner_rating": partnerRating,
    "partner_rating_order": partnerRatingOrder,
    "partner_rating_product": partnerRatingProduct,
    "partner_rating_pricing": partnerRatingPricing,
    "partner_rating_friendliness": partnerRatingFriendliness,
    "partner_feedback": partnerFeedback,
    "partner_office_hours": List<dynamic>.from(partnerOfficeHours.map((x) => x.toJson())),
    "is_barcode": isBarcode,
    "barcode": barcode,
  };
}

class Category {
  int id;
  int option;
  int parentId;
  String name;
  String icon;
  DateTime createdAt;
  DateTime updatedAt;
  Pivot pivot;

  Category({
    this.id,
    this.option,
    this.parentId,
    this.name,
    this.icon,
    this.createdAt,
    this.updatedAt,
    this.pivot,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    option: json["option"],
    parentId: json["parent_id"],
    name: json["name"],
    icon: json["icon"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    pivot: Pivot.fromJson(json["pivot"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "option": option,
    "parent_id": parentId,
    "name": name,
    "icon": icon,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "pivot": pivot.toJson(),
  };
}

class Pivot {
  int promotionRegularId;
  int categoriId;

  Pivot({
    this.promotionRegularId,
    this.categoriId,
  });

  factory Pivot.fromJson(Map<String, dynamic> json) => Pivot(
    promotionRegularId: json["promotion_regular_id"],
    categoriId: json["categori_id"],
  );

  Map<String, dynamic> toJson() => {
    "promotion_regular_id": promotionRegularId,
    "categori_id": categoriId,
  };
}

class PartnerOfficeHour {
  String day;
  int close;
  int allDayLong;
  String from;
  String to;

  PartnerOfficeHour({
    this.day,
    this.close,
    this.allDayLong,
    this.from,
    this.to,
  });

  factory PartnerOfficeHour.fromJson(Map<String, dynamic> json) => PartnerOfficeHour(
    day: json["day"],
    close: json["close"],
    allDayLong: json["all_day_long"],
    from: json["from"],
    to: json["to"],
  );

  Map<String, dynamic> toJson() => {
    "day": day,
    "close": close,
    "all_day_long": allDayLong,
    "from": from,
    "to": to,
  };
}

class Meta {
  Pagination pagination;

  Meta({
    this.pagination,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    pagination: Pagination.fromJson(json["pagination"]),
  );

  Map<String, dynamic> toJson() => {
    "pagination": pagination.toJson(),
  };
}

class Pagination {
  int total;
  int count;
  int perPage;
  int currentPage;
  int totalPages;
  Links links;

  Pagination({
    this.total,
    this.count,
    this.perPage,
    this.currentPage,
    this.totalPages,
    this.links,
  });

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
    total: json["total"],
    count: json["count"],
    perPage: json["per_page"],
    currentPage: json["current_page"],
    totalPages: json["total_pages"],
    links: Links.fromJson(json["links"]),
  );

  Map<String, dynamic> toJson() => {
    "total": total,
    "count": count,
    "per_page": perPage,
    "current_page": currentPage,
    "total_pages": totalPages,
    "links": links.toJson(),
  };
}

class Links {
  Links();

  factory Links.fromJson(Map<String, dynamic> json) => Links(
  );

  Map<String, dynamic> toJson() => {
  };
}
