// To parse this JSON data, do
//
//     final categoryDataRemote = categoryDataRemoteFromJson(jsonString);

import 'dart:convert';

CategoryDataRemote categoryDataRemoteFromJson(String str) => CategoryDataRemote.fromJson(json.decode(str));

String categoryDataRemoteToJson(CategoryDataRemote data) => json.encode(data.toJson());

class CategoryDataRemote {
  bool status;
  String message;
  List<Datum> data;
  Meta meta;
  List<dynamic> error;

  CategoryDataRemote({
    this.status,
    this.message,
    this.data,
    this.meta,
    this.error,
  });

  factory CategoryDataRemote.fromJson(Map<String, dynamic> json) => CategoryDataRemote(
    status: json["status"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    meta: Meta.fromJson(json["meta"]),
    error: List<dynamic>.from(json["error"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "meta": meta.toJson(),
    "error": List<dynamic>.from(error.map((x) => x)),
  };
}

class Datum {
  int id;
  String name;
  int parentId;
  String icon;
  bool optionMuslim;

  Datum({
    this.id,
    this.name,
    this.parentId,
    this.icon,
    this.optionMuslim,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    name: json["name"],
    parentId: json["parent_id"],
    icon: json["icon"],
    optionMuslim: json["option_muslim"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "parent_id": parentId,
    "icon": icon,
    "option_muslim": optionMuslim,
  };
}

class Meta {
  Meta();

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
  );

  Map<String, dynamic> toJson() => {
  };
}
