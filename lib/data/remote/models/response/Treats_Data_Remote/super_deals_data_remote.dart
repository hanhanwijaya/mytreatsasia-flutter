// To parse this JSON data, do
//
//     final superDealsDataRemote = superDealsDataRemoteFromJson(jsonString);

import 'dart:convert';

SuperDealsDataRemote superDealsDataRemoteFromJson(String str) => SuperDealsDataRemote.fromJson(json.decode(str));

String superDealsDataRemoteToJson(SuperDealsDataRemote data) => json.encode(data.toJson());

class SuperDealsDataRemote {
  bool status;
  String message;
  List<Datum> data;
  Meta meta;
  List<dynamic> error;

  SuperDealsDataRemote({
    this.status,
    this.message,
    this.data,
    this.meta,
    this.error,
  });

  factory SuperDealsDataRemote.fromJson(Map<String, dynamic> json) => SuperDealsDataRemote(
    status: json["status"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    meta: Meta.fromJson(json["meta"]),
    error: List<dynamic>.from(json["error"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "meta": meta.toJson(),
    "error": List<dynamic>.from(error.map((x) => x)),
  };
}

class Datum {
  int id;
  String title;
  String description;
  List<Image> image;
  String category;
  int originalRetailPrice;
  int mtSubsidy;
  int maxQuantity;
  int maxUsed;
  DateTime publishDate;
  List<Image> legalAggreement;
  String superdealId;
  dynamic signature;
  String status;
  bool favorite;
  int partnerId;
  String partnerName;
  String partnerAvatar;
  String partnerAddress;
  String partnerLatitude;
  String partnerLongitude;
  String partnerSignature;
  double partnerRating;
  double partnerRatingOrder;
  double partnerRatingProduct;
  double partnerRatingPricing;
  double partnerRatingFriendliness;
  int partnerFeedback;
  List<PartnerOfficeHour> partnerOfficeHours;

  Datum({
    this.id,
    this.title,
    this.description,
    this.image,
    this.category,
    this.originalRetailPrice,
    this.mtSubsidy,
    this.maxQuantity,
    this.maxUsed,
    this.publishDate,
    this.legalAggreement,
    this.superdealId,
    this.signature,
    this.status,
    this.favorite,
    this.partnerId,
    this.partnerName,
    this.partnerAvatar,
    this.partnerAddress,
    this.partnerLatitude,
    this.partnerLongitude,
    this.partnerSignature,
    this.partnerRating,
    this.partnerRatingOrder,
    this.partnerRatingProduct,
    this.partnerRatingPricing,
    this.partnerRatingFriendliness,
    this.partnerFeedback,
    this.partnerOfficeHours,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    title: json["title"],
    description: json["description"],
    image: List<Image>.from(json["image"].map((x) => Image.fromJson(x))),
    category: json["category"],
    originalRetailPrice: json["original_retail_price"],
    mtSubsidy: json["mt_subsidy"],
    maxQuantity: json["max_quantity"],
    maxUsed: json["max_used"],
    publishDate: DateTime.parse(json["publish_date"]),
    legalAggreement: List<Image>.from(json["legal_aggreement"].map((x) => Image.fromJson(x))),
    superdealId: json["superdeal_id"],
    signature: json["signature"],
    status: json["status"],
    favorite: json["favorite"],
    partnerId: json["partner_id"],
    partnerName: json["partner_name"],
    partnerAvatar: json["partner_avatar"],
    partnerAddress: json["partner_address"],
    partnerLatitude: json["partner_latitude"],
    partnerLongitude: json["partner_longitude"],
    partnerSignature: json["partner_signature"],
    partnerRating: json["partner_rating"].toDouble(),
    partnerRatingOrder: json["partner_rating_order"].toDouble(),
    partnerRatingProduct: json["partner_rating_product"].toDouble(),
    partnerRatingPricing: json["partner_rating_pricing"].toDouble(),
    partnerRatingFriendliness: json["partner_rating_friendliness"].toDouble(),
    partnerFeedback: json["partner_feedback"],
    partnerOfficeHours: List<PartnerOfficeHour>.from(json["partner_office_hours"].map((x) => PartnerOfficeHour.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "description": description,
    "image": List<dynamic>.from(image.map((x) => x.toJson())),
    "category": category,
    "original_retail_price": originalRetailPrice,
    "mt_subsidy": mtSubsidy,
    "max_quantity": maxQuantity,
    "max_used": maxUsed,
    "publish_date": "${publishDate.year.toString().padLeft(4, '0')}-${publishDate.month.toString().padLeft(2, '0')}-${publishDate.day.toString().padLeft(2, '0')}",
    "legal_aggreement": List<dynamic>.from(legalAggreement.map((x) => x.toJson())),
    "superdeal_id": superdealId,
    "signature": signature,
    "status": status,
    "favorite": favorite,
    "partner_id": partnerId,
    "partner_name": partnerName,
    "partner_avatar": partnerAvatar,
    "partner_address": partnerAddress,
    "partner_latitude": partnerLatitude,
    "partner_longitude": partnerLongitude,
    "partner_signature": partnerSignature,
    "partner_rating": partnerRating,
    "partner_rating_order": partnerRatingOrder,
    "partner_rating_product": partnerRatingProduct,
    "partner_rating_pricing": partnerRatingPricing,
    "partner_rating_friendliness": partnerRatingFriendliness,
    "partner_feedback": partnerFeedback,
    "partner_office_hours": List<dynamic>.from(partnerOfficeHours.map((x) => x.toJson())),
  };
}

class Image {
  String name;
  String file;

  Image({
    this.name,
    this.file,
  });

  factory Image.fromJson(Map<String, dynamic> json) => Image(
    name: json["name"],
    file: json["file"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "file": file,
  };
}

class PartnerOfficeHour {
  String day;
  int close;
  int allDayLong;
  String from;
  String to;

  PartnerOfficeHour({
    this.day,
    this.close,
    this.allDayLong,
    this.from,
    this.to,
  });

  factory PartnerOfficeHour.fromJson(Map<String, dynamic> json) => PartnerOfficeHour(
    day: json["day"],
    close: json["close"],
    allDayLong: json["all_day_long"],
    from: json["from"],
    to: json["to"],
  );

  Map<String, dynamic> toJson() => {
    "day": day,
    "close": close,
    "all_day_long": allDayLong,
    "from": from,
    "to": to,
  };
}

class Meta {
  Pagination pagination;

  Meta({
    this.pagination,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    pagination: Pagination.fromJson(json["pagination"]),
  );

  Map<String, dynamic> toJson() => {
    "pagination": pagination.toJson(),
  };
}

class Pagination {
  int total;
  int count;
  int perPage;
  int currentPage;
  int totalPages;
  Links links;

  Pagination({
    this.total,
    this.count,
    this.perPage,
    this.currentPage,
    this.totalPages,
    this.links,
  });

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
    total: json["total"],
    count: json["count"],
    perPage: json["per_page"],
    currentPage: json["current_page"],
    totalPages: json["total_pages"],
    links: Links.fromJson(json["links"]),
  );

  Map<String, dynamic> toJson() => {
    "total": total,
    "count": count,
    "per_page": perPage,
    "current_page": currentPage,
    "total_pages": totalPages,
    "links": links.toJson(),
  };
}

class Links {
  Links();

  factory Links.fromJson(Map<String, dynamic> json) => Links(
  );

  Map<String, dynamic> toJson() => {
  };
}
