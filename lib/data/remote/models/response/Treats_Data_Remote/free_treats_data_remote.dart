// To parse this JSON data, do
//
//     final freeTreatsDataRemote = freeTreatsDataRemoteFromJson(jsonString);

import 'dart:convert';

FreeTreatsDataRemote freeTreatsDataRemoteFromJson(String str) => FreeTreatsDataRemote.fromJson(json.decode(str));

String freeTreatsDataRemoteToJson(FreeTreatsDataRemote data) => json.encode(data.toJson());

class FreeTreatsDataRemote {
  bool status;
  String message;
  List<Datum> data;
  Meta meta;
  List<dynamic> error;

  FreeTreatsDataRemote({
    this.status,
    this.message,
    this.data,
    this.meta,
    this.error,
  });

  factory FreeTreatsDataRemote.fromJson(Map<String, dynamic> json) => FreeTreatsDataRemote(
    status: json["status"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    meta: Meta.fromJson(json["meta"]),
    error: List<dynamic>.from(json["error"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "meta": meta.toJson(),
    "error": List<dynamic>.from(error.map((x) => x)),
  };
}

class Datum {
  int id;
  String title;
  String picture;
  bool muslimFriendly;
  int maxCustomer;
  int originalPrice;
  Partner partner;
  DateTime startDate;
  DateTime endDate;
  String description;
  List<String> category;
  bool favorite;

  Datum({
    this.id,
    this.title,
    this.picture,
    this.muslimFriendly,
    this.maxCustomer,
    this.originalPrice,
    this.partner,
    this.startDate,
    this.endDate,
    this.description,
    this.category,
    this.favorite,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    title: json["title"],
    picture: json["picture"],
    muslimFriendly: json["muslim_friendly"],
    maxCustomer: json["max_customer"],
    originalPrice: json["original_price"],
    partner: Partner.fromJson(json["partner"]),
    startDate: DateTime.parse(json["start_date"]),
    endDate: DateTime.parse(json["end_date"]),
    description: json["description"],
    category: List<String>.from(json["category"].map((x) => x)),
    favorite: json["favorite"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "picture": picture,
    "muslim_friendly": muslimFriendly,
    "max_customer": maxCustomer,
    "original_price": originalPrice,
    "partner": partner.toJson(),
    "start_date": "${startDate.year.toString().padLeft(4, '0')}-${startDate.month.toString().padLeft(2, '0')}-${startDate.day.toString().padLeft(2, '0')}",
    "end_date": "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
    "description": description,
    "category": List<dynamic>.from(category.map((x) => x)),
    "favorite": favorite,
  };
}

class Partner {
  int id;
  String name;
  String avatar;
  double rating;
  double ratingOrder;
  double ratingProduct;
  double ratingPricing;
  double ratingFriendliness;
  String signature;
  int feedback;
  String address;
  String latitude;
  String longitude;
  List<OfficeHour> officeHours;

  Partner({
    this.id,
    this.name,
    this.avatar,
    this.rating,
    this.ratingOrder,
    this.ratingProduct,
    this.ratingPricing,
    this.ratingFriendliness,
    this.signature,
    this.feedback,
    this.address,
    this.latitude,
    this.longitude,
    this.officeHours,
  });

  factory Partner.fromJson(Map<String, dynamic> json) => Partner(
    id: json["id"],
    name: json["name"],
    avatar: json["avatar"],
    rating: json["rating"].toDouble(),
    ratingOrder: json["rating_order"].toDouble(),
    ratingProduct: json["rating_product"].toDouble(),
    ratingPricing: json["rating_pricing"].toDouble(),
    ratingFriendliness: json["rating_friendliness"].toDouble(),
    signature: json["signature"],
    feedback: json["feedback"],
    address: json["address"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    officeHours: List<OfficeHour>.from(json["office_hours"].map((x) => OfficeHour.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "avatar": avatar,
    "rating": rating,
    "rating_order": ratingOrder,
    "rating_product": ratingProduct,
    "rating_pricing": ratingPricing,
    "rating_friendliness": ratingFriendliness,
    "signature": signature,
    "feedback": feedback,
    "address": address,
    "latitude": latitude,
    "longitude": longitude,
    "office_hours": List<dynamic>.from(officeHours.map((x) => x.toJson())),
  };
}

class OfficeHour {
  String day;
  int close;
  int allDayLong;
  String from;
  String to;

  OfficeHour({
    this.day,
    this.close,
    this.allDayLong,
    this.from,
    this.to,
  });

  factory OfficeHour.fromJson(Map<String, dynamic> json) => OfficeHour(
    day: json["day"],
    close: json["close"],
    allDayLong: json["all_day_long"],
    from: json["from"],
    to: json["to"],
  );

  Map<String, dynamic> toJson() => {
    "day": day,
    "close": close,
    "all_day_long": allDayLong,
    "from": from,
    "to": to,
  };
}

class Meta {
  Pagination pagination;

  Meta({
    this.pagination,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    pagination: Pagination.fromJson(json["pagination"]),
  );

  Map<String, dynamic> toJson() => {
    "pagination": pagination.toJson(),
  };
}

class Pagination {
  int total;
  int count;
  int perPage;
  int currentPage;
  int totalPages;
  Links links;

  Pagination({
    this.total,
    this.count,
    this.perPage,
    this.currentPage,
    this.totalPages,
    this.links,
  });

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
    total: json["total"],
    count: json["count"],
    perPage: json["per_page"],
    currentPage: json["current_page"],
    totalPages: json["total_pages"],
    links: Links.fromJson(json["links"]),
  );

  Map<String, dynamic> toJson() => {
    "total": total,
    "count": count,
    "per_page": perPage,
    "current_page": currentPage,
    "total_pages": totalPages,
    "links": links.toJson(),
  };
}

class Links {
  Links();

  factory Links.fromJson(Map<String, dynamic> json) => Links(
  );

  Map<String, dynamic> toJson() => {
  };
}
