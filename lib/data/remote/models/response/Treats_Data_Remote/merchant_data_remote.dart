// To parse this JSON data, do
//
//     final merchantDataRemote = merchantDataRemoteFromJson(jsonString);

import 'dart:convert';

MerchantDataRemote merchantDataRemoteFromJson(String str) => MerchantDataRemote.fromJson(json.decode(str));

String merchantDataRemoteToJson(MerchantDataRemote data) => json.encode(data.toJson());

class MerchantDataRemote {
  bool status;
  String message;
  List<Datum> data;
  Meta meta;
  List<dynamic> error;

  MerchantDataRemote({
    this.status,
    this.message,
    this.data,
    this.meta,
    this.error,
  });

  factory MerchantDataRemote.fromJson(Map<String, dynamic> json) => MerchantDataRemote(
    status: json["status"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    meta: Meta.fromJson(json["meta"]),
    error: List<dynamic>.from(json["error"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "meta": meta.toJson(),
    "error": List<dynamic>.from(error.map((x) => x)),
  };
}

class Datum {
  int id;
  String name;
  String avatar;
  String address;
  String country;
  String city;
  String subdistrict;
  String latitude;
  String longitude;
  dynamic distance;
  String description;
  double rating;
  double ratingOrder;
  double ratingProduct;
  double ratingPricing;
  double ratingFriendliness;
  int feedback;
  List<String> storeImages;
  List<OfficeHour> officeHours;

  Datum({
    this.id,
    this.name,
    this.avatar,
    this.address,
    this.country,
    this.city,
    this.subdistrict,
    this.latitude,
    this.longitude,
    this.distance,
    this.description,
    this.rating,
    this.ratingOrder,
    this.ratingProduct,
    this.ratingPricing,
    this.ratingFriendliness,
    this.feedback,
    this.storeImages,
    this.officeHours,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    name: json["name"],
    avatar: json["avatar"],
    address: json["address"],
    country: json["country"],
    city: json["city"],
    subdistrict: json["subdistrict"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    distance: json["distance"],
    description: json["description"],
    rating: json["rating"].toDouble(),
    ratingOrder: json["rating_order"].toDouble(),
    ratingProduct: json["rating_product"].toDouble(),
    ratingPricing: json["rating_pricing"].toDouble(),
    ratingFriendliness: json["rating_friendliness"].toDouble(),
    feedback: json["feedback"],
    storeImages: List<String>.from(json["store_images"].map((x) => x)),
    officeHours: List<OfficeHour>.from(json["office_hours"].map((x) => OfficeHour.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "avatar": avatar,
    "address": address,
    "country": country,
    "city": city,
    "subdistrict": subdistrict,
    "latitude": latitude,
    "longitude": longitude,
    "distance": distance,
    "description": description,
    "rating": rating,
    "rating_order": ratingOrder,
    "rating_product": ratingProduct,
    "rating_pricing": ratingPricing,
    "rating_friendliness": ratingFriendliness,
    "feedback": feedback,
    "store_images": List<dynamic>.from(storeImages.map((x) => x)),
    "office_hours": List<dynamic>.from(officeHours.map((x) => x.toJson())),
  };
}

class OfficeHour {
  int id;
  int partnerId;
  String day;
  int close;
  int allDayLong;
  String from;
  String to;
  DateTime createdAt;
  DateTime updatedAt;

  OfficeHour({
    this.id,
    this.partnerId,
    this.day,
    this.close,
    this.allDayLong,
    this.from,
    this.to,
    this.createdAt,
    this.updatedAt,
  });

  factory OfficeHour.fromJson(Map<String, dynamic> json) => OfficeHour(
    id: json["id"],
    partnerId: json["partner_id"],
    day: json["day"],
    close: json["close"],
    allDayLong: json["all_day_long"],
    from: json["from"],
    to: json["to"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "partner_id": partnerId,
    "day": day,
    "close": close,
    "all_day_long": allDayLong,
    "from": from,
    "to": to,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}

class Meta {
  Pagination pagination;

  Meta({
    this.pagination,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    pagination: Pagination.fromJson(json["pagination"]),
  );

  Map<String, dynamic> toJson() => {
    "pagination": pagination.toJson(),
  };
}

class Pagination {
  int total;
  int count;
  int perPage;
  int currentPage;
  int totalPages;
  Links links;

  Pagination({
    this.total,
    this.count,
    this.perPage,
    this.currentPage,
    this.totalPages,
    this.links,
  });

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
    total: json["total"],
    count: json["count"],
    perPage: json["per_page"],
    currentPage: json["current_page"],
    totalPages: json["total_pages"],
    links: Links.fromJson(json["links"]),
  );

  Map<String, dynamic> toJson() => {
    "total": total,
    "count": count,
    "per_page": perPage,
    "current_page": currentPage,
    "total_pages": totalPages,
    "links": links.toJson(),
  };
}

class Links {
  Links();

  factory Links.fromJson(Map<String, dynamic> json) => Links(
  );

  Map<String, dynamic> toJson() => {
  };
}
