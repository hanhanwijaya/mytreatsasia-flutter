// To parse this JSON data, do
//
//     final eventDataRemote = eventDataRemoteFromJson(jsonString);

import 'dart:convert';

EventDataRemote eventDataRemoteFromJson(String str) => EventDataRemote.fromJson(json.decode(str));

String eventDataRemoteToJson(EventDataRemote data) => json.encode(data.toJson());

class EventDataRemote {
  bool status;
  String message;
  List<Datum> data;
  Meta meta;
  List<dynamic> error;

  EventDataRemote({
    this.status,
    this.message,
    this.data,
    this.meta,
    this.error,
  });

  factory EventDataRemote.fromJson(Map<String, dynamic> json) => EventDataRemote(
    status: json["status"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    meta: Meta.fromJson(json["meta"]),
    error: List<dynamic>.from(json["error"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "meta": meta.toJson(),
    "error": List<dynamic>.from(error.map((x) => x)),
  };
}

class Datum {
  int id;
  String title;
  DateTime period;
  String picture;
  String description;
  Location location;

  Datum({
    this.id,
    this.title,
    this.period,
    this.picture,
    this.description,
    this.location,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    title: json["title"],
    period: DateTime.parse(json["period"]),
    picture: json["picture"],
    description: json["description"],
    location: Location.fromJson(json["location"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "period": period.toIso8601String(),
    "picture": picture,
    "description": description,
    "location": location.toJson(),
  };
}

class Location {
  String address;
  String country;
  String zipcode;
  String subdistrict;

  Location({
    this.address,
    this.country,
    this.zipcode,
    this.subdistrict,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
    address: json["address"],
    country: json["country"],
    zipcode: json["zipcode"],
    subdistrict: json["subdistrict"],
  );

  Map<String, dynamic> toJson() => {
    "address": address,
    "country": country,
    "zipcode": zipcode,
    "subdistrict": subdistrict,
  };
}

class Meta {
  Pagination pagination;

  Meta({
    this.pagination,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    pagination: Pagination.fromJson(json["pagination"]),
  );

  Map<String, dynamic> toJson() => {
    "pagination": pagination.toJson(),
  };
}

class Pagination {
  int total;
  int count;
  int perPage;
  int currentPage;
  int totalPages;
  Links links;

  Pagination({
    this.total,
    this.count,
    this.perPage,
    this.currentPage,
    this.totalPages,
    this.links,
  });

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
    total: json["total"],
    count: json["count"],
    perPage: json["per_page"],
    currentPage: json["current_page"],
    totalPages: json["total_pages"],
    links: Links.fromJson(json["links"]),
  );

  Map<String, dynamic> toJson() => {
    "total": total,
    "count": count,
    "per_page": perPage,
    "current_page": currentPage,
    "total_pages": totalPages,
    "links": links.toJson(),
  };
}

class Links {
  Links();

  factory Links.fromJson(Map<String, dynamic> json) => Links(
  );

  Map<String, dynamic> toJson() => {
  };
}
