// To parse this JSON data, do
//
//     final bannerDataRemote = bannerDataRemoteFromJson(jsonString);

import 'dart:convert';

BannerDataRemote bannerDataRemoteFromJson(String str) => BannerDataRemote.fromJson(json.decode(str));

String bannerDataRemoteToJson(BannerDataRemote data) => json.encode(data.toJson());

class BannerDataRemote {
  bool status;
  String message;
  List<Datum> data;
  Meta meta;
  List<dynamic> error;

  BannerDataRemote({
    this.status,
    this.message,
    this.data,
    this.meta,
    this.error,
  });

  factory BannerDataRemote.fromJson(Map<String, dynamic> json) => BannerDataRemote(
    status: json["status"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    meta: Meta.fromJson(json["meta"]),
    error: List<dynamic>.from(json["error"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "meta": meta.toJson(),
    "error": List<dynamic>.from(error.map((x) => x)),
  };
}

class Datum {
  int id;
  String title;
  String periodStart;
  String periodEnd;
  String picture;
  String description;
  String qrCode;
  int flFrontend;

  Datum({
    this.id,
    this.title,
    this.periodStart,
    this.periodEnd,
    this.picture,
    this.description,
    this.qrCode,
    this.flFrontend,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    title: json["title"],
    periodStart: json["period_start"],
    periodEnd: json["period_end"],
    picture: json["picture"],
    description: json["description"],
    qrCode: json["qr_code"],
    flFrontend: json["fl_frontend"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "period_start": periodStart,
    "period_end": periodEnd,
    "picture": picture,
    "description": description,
    "qr_code": qrCode,
    "fl_frontend": flFrontend,
  };
}

class Meta {
  Pagination pagination;

  Meta({
    this.pagination,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    pagination: Pagination.fromJson(json["pagination"]),
  );

  Map<String, dynamic> toJson() => {
    "pagination": pagination.toJson(),
  };
}

class Pagination {
  int total;
  int count;
  int perPage;
  int currentPage;
  int totalPages;
  Links links;

  Pagination({
    this.total,
    this.count,
    this.perPage,
    this.currentPage,
    this.totalPages,
    this.links,
  });

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
    total: json["total"],
    count: json["count"],
    perPage: json["per_page"],
    currentPage: json["current_page"],
    totalPages: json["total_pages"],
    links: Links.fromJson(json["links"]),
  );

  Map<String, dynamic> toJson() => {
    "total": total,
    "count": count,
    "per_page": perPage,
    "current_page": currentPage,
    "total_pages": totalPages,
    "links": links.toJson(),
  };
}

class Links {
  Links();

  factory Links.fromJson(Map<String, dynamic> json) => Links(
  );

  Map<String, dynamic> toJson() => {
  };
}
