import 'package:built_value/built_value.dart';

part 'team_list_data.g.dart';

abstract class TeamListData
    implements Built<TeamListData, TeamListDataBuilder> {
  List<TeamData> get teams;

  TeamListData._();

  factory TeamListData([updates(TeamListDataBuilder b)]) = _$TeamListData;
}

abstract class TeamData implements Built<TeamData, TeamDataBuilder> {
  String get idTeam;

  String get idSoccerXML;

  String get intLoved;

  String get strTeam;

  String get strTeamShort;

  String get strAlternate;

  String get intFormedYear;

  String get strSport;

  String get strLeague;

  String get idLeague;

  String get strDivision;

  String get strManager;

  String get strStadium;

  String get strKeywords;

  String get strRSS;

  String get strStadiumThumb;

  String get strStadiumDescription;

  String get strStadiumLocation;

  String get intStadiumCapacity;

  String get strWebsite;

  String get strFacebook;

  String get strTwitter;

  String get strInstagram;

  String get strDescriptionEN;

  String get strDescriptionDE;

  String get strDescriptionFR;

  String get strDescriptionCN;

  String get strDescriptionIT;

  String get strDescriptionJP;

  String get strDescriptionRU;

  String get strDescriptionES;

  String get strDescriptionPT;

  String get strDescriptionSE;

  String get strDescriptionNL;

  String get strDescriptionHU;

  String get strDescriptionNO;

  String get strDescriptionIL;

  String get strDescriptionPL;

  String get strGender;

  String get strCountry;

  String get strTeamBadge;

  String get strTeamJersey;

  String get strTeamLogo;

  String get strTeamFanart1;

  String get strTeamFanart2;

  String get strTeamFanart3;

  String get strTeamFanart4;

  String get strTeamBanner;

  String get strYoutube;

  String get strLocked;

  TeamData._();

  factory TeamData([updates(TeamDataBuilder b)]) = _$TeamData;
}
