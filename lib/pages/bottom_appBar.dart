import 'package:asia/presentation/ui/team_list/team_list_page.dart';
import 'package:asia/presentation/ui/treats_Page/treats_list_page.dart';
import 'package:badges/badges.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vibration/vibration.dart';

class BottomBar extends StatefulWidget {
  BottomBar({this.user, this.googleSignIn, this.name});

  final FirebaseUser user;
  final GoogleSignIn googleSignIn;
  final name;

  @override
  _BottomBarState createState() => _BottomBarState();

//  method() => createState()._signIn();
}

class _BottomBarState extends State<BottomBar> {
  Position _currentPosition;

//  String name ;
//
//  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
//  final GoogleSignIn googleSignIn = new GoogleSignIn();
//
//  Future<FirebaseUser> _signIn() async {
//    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
//    GoogleSignInAuthentication googleSignInAuthentication =
//        await googleSignInAccount.authentication;
//
//    final AuthCredential credential = GoogleAuthProvider.getCredential(
//        idToken: googleSignInAuthentication.idToken,
//        accessToken: googleSignInAuthentication.accessToken);
//    final AuthResult authResult =
//        await firebaseAuth.signInWithCredential(credential);
//    final FirebaseUser firebaseUser = authResult.user;
//
//    print('yes succes${firebaseUser.displayName}');
//
//
//
//    name = firebaseUser.displayName;
//
//    TeamListPage(name: name,);
//    Navigator.push(
//          context, MaterialPageRoute(builder: (context) => ProfileBasePage(user: firebaseUser, googleSignIn: googleSignIn,)));
//
//    return firebaseUser;
//  }

  @override
  void initState() {
    super.initState();
    // this will help us fetch new data from the server
//    widget.bloc.fetchNewDataSink.add(Event());
    _getCurrentLocation();
  }

//  Page Properties
  int currentTab = 0;

//  Tab Views
  final List<Widget> screens = [
    TeamListPage(),
//    SavePage(),
//    NearbyPage(),
//    InboxBasePage(),
//    FreedomBasePage(),
  ];

//  Active Page Tab View
  Widget _currentScreen = TreatsBasePage(
    lat: '-6.8748062',
    long: '107.5857363',
  );

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
//    Widget _currentScreen = TeamListPage(lat: _currentPosition.latitude.toString(), long: _currentPosition.longitude.toString());
    return Scaffold(
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(children: <Widget>[
            PageStorage(child: _currentScreen, bucket: bucket),
            Positioned(
              bottom: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 60.0,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 3.0,
                    ),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: MaterialButton(
                        minWidth: 40,
                        onPressed: () {
                          Vibration.vibrate(duration: 50);
                          setState(() {
                            _currentScreen = TreatsBasePage(
                              lat: _currentPosition.latitude.toString(),
                              long: _currentPosition.longitude.toString(),
                            );
                            currentTab = 0;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: currentTab == 0
                                      ? Color.fromRGBO(133, 92, 214, 0.24)
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(8)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SvgPicture.asset(
                                    'images/bottombar/ic_Treats.svg',
                                    color: currentTab == 0
                                        ? Color(0xff855CD6)
                                        : Colors.grey,
                                    width: 16,
                                    height: 16,
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              'Treats',
                              style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: 10,
                                color: currentTab == 0
                                    ? Color(0xff855CD6)
                                    : Colors.grey,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: MaterialButton(
                        minWidth: 40,
                        onPressed: () {
                          Vibration.vibrate(duration: 50);
                          setState(() {
                            _currentScreen = TeamListPage(
                                lat: _currentPosition.latitude.toString(),
                                long: _currentPosition.longitude.toString());
                            currentTab = 1;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: currentTab == 1
                                      ? Color.fromRGBO(133, 92, 214, 0.24)
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(8)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SvgPicture.asset(
                                    'images/bottombar/ic_Save.svg',
                                    color: currentTab == 1
                                        ? Color(0xff855CD6)
                                        : Colors.grey,
                                    width: 16,
                                    height: 16,
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              'Save',
                              style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: 10,
                                color: currentTab == 1
                                    ? Color(0xff855CD6)
                                    : Colors.grey,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: MaterialButton(
                        onPressed: (){

                        },
                        minWidth: 40,

                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.dashboard, color: Colors.transparent),
                            Text(
                              'AR',
                              style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: 10,
                                color: currentTab == 2
                                    ? Color(0xff855CD6)
                                    : Colors.grey,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: MaterialButton(
                        minWidth: 40,
                        onPressed: () {
                          Vibration.vibrate(duration: 50);
                          setState(() {
                            _currentScreen = TeamListPage();
                            currentTab = 4;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: currentTab == 4
                                      ? Color.fromRGBO(133, 92, 214, 0.24)
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(8)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SvgPicture.asset(
                                    'images/bottombar/ic_Freedom.svg',
                                    color: currentTab == 4
                                        ? Color(0xff855CD6)
                                        : Colors.grey,
                                    width: 16,
                                    height: 16,
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              'Freedom',
                              style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: 10,
                                color: currentTab == 4
                                    ? Color(0xff855CD6)
                                    : Colors.grey,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: MaterialButton(
                        minWidth: 40,
                        onPressed: () {
                          Vibration.vibrate(duration: 50);
                          setState(() {
                            _currentScreen = TeamListPage();
                            currentTab = 3;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: currentTab == 3
                                      ? Color.fromRGBO(133, 92, 214, 0.24)
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(8)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Badge(
                                    badgeContent: Text(
                                      '3',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 10),
                                    ),
                                    child: SvgPicture.asset(
                                      'images/bottombar/ic_Inbox.svg',
                                      color: currentTab == 3
                                          ? Color(0xff855CD6)
                                          : Colors.grey,
                                      width: 16,
                                      height: 16,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              'Inbox',
                              style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: 10,
                                color: currentTab == 3
                                    ? Color(0xff855CD6)
                                    : Colors.grey,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 24,
//           left: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: Center(
                  child: MaterialButton(
                    onPressed: (){
                      Vibration.vibrate(duration: 50);
                      setState(() {
//                        _currentScreen = NearbyListPage(
//                            lat: _currentPosition.latitude.toString(),
//                            long: _currentPosition.longitude.toString());
                        currentTab = 2;
                      });
                    },
                    child: Container(
                      width: 48,
                      height: 48,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            stops: [
                              0.2,
                              1,
                            ],
                            colors: [
                              Color(0xff563399),
                              Color(0xff9E85BF),
                            ]),
                      ),
                      child: Container(
                          width: 24,
                          height: 24,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SvgPicture.asset('images/bottombar/ic_Ar.svg', fit: BoxFit.cover,),
                            ],
                          )),
                    ),
                  ),
                ),
              ),
            )
          ])),
//      FAB Button

//      Bottom App Bar
//      bottomNavigationBar: BottomAppBar(
//        shape: CircularNotchedRectangle(),
//
//      ),
    );
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
    }).catchError((e) {
      print(e);
    });
  }
}

//class HomePage extends StatefulWidget {
//  @override
//  _HomePageState createState() => _HomePageState();
//}
//
//class _HomePageState extends State<HomePage> {
//  Position _currentPosition;
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text("Location"),
//      ),
//      body: Center(
//        child: Column(
//          mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            if (_currentPosition != null)
//              Text(
//                  "LAT: ${_currentPosition.latitude}, LNG: ${_currentPosition.longitude}"),
//            FlatButton(
//              child: Text("Get location"),
//              onPressed: () {
//                _getCurrentLocation();
//              },
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//
//  _getCurrentLocation() {
//    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
//
//    geolocator
//        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
//        .then((Position position) {
//      setState(() {
//        _currentPosition = position;
//      });
//    }).catchError((e) {
//      print(e);
//    });
//  }
//}